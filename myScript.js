"use strict";

const { chromium } = require('playwright');

(async () => {
  const browser = await chromium.launch({headless: false});
  const context = await browser.newContext();
  const page = await context.newPage();
  await page.goto("https://google.com");
  await page.waitForSelector("#footer", {visibility: "visible"});
  await page.waitForSelector("#hplogo", {visibility: "visible"});
  await page.type(".gLFyf", 'test');
  await sendSpecialCharacter(page, ".gLFyf", 'Enter');
  await page.click("#rso > div:nth-child(1) > div > div.yuRUbf > a > h3 > span");
  await waitForText(page, ".main-menu-link", 'Apps');
  await page.click(".main-menu-link", {button: "right"});
  await browser.close();
})();

// move to utils.js

async function waitForText(page, selector, expectedText) {
  await page.waitForFunction(([selector, expectedText]) => {
    const element = document.querySelector(selector);
    return element && element.textContent.replace(/[\r\n]+/g, "").trim() === expectedText.trim();
  }, [selector, expectedText]);
}

async function sendSpecialCharacter(page, selector, key) {
  const elementHandle = await page.$(selector);
  await elementHandle.press(key);
}

